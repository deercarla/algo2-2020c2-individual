#include "Lista.h"

Lista::Lista():head(nullptr),tail(nullptr){}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    Nodo* nodo = head;
    while(nodo != NULL)
    {
        nodo = nodo->next;
        delete(head);
        head = nodo;
    }

}

Lista& Lista::operator=(const Lista& aCopiar) {
    if(longitud()==0) {
        for(int i=0; i<aCopiar.longitud(); i++){
            agregarAtras(aCopiar.iesimo(i));
        }
    }else{
        Nodo* temp = head->next;
        while(temp != NULL)
        {
            temp = temp->next;
            delete(head);
            head = temp;
        }
        for(int i=0; i<aCopiar.longitud(); i++){
            agregarAtras(aCopiar.iesimo(i));
        }
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* node = new Nodo();
    node->dato = elem;
    if(longitud()==0){
        head=node;
        tail=node;
    }else{
        Nodo* head_vieja = head;
        node->next = this->head;
        this->head = node;
        head->prev = nullptr;
        head_vieja->prev=head;
    }
}

void Lista::agregarAtras(const int& elem) {
    Nodo* node = new Nodo();
    node->dato = elem;
    if(longitud()==0){
        head=node;
        tail=node;
    }else{
        Nodo* tail_vieja= tail;
        tail->next= node;
        this->tail = node;
        node->next = nullptr;
        node-> prev=tail_vieja;
    }
}

void Lista::eliminar(Nat i) {
    int j = 0;
    Nodo* nodo = head;
    while(j < i) {
        nodo=nodo->next;
        j++;
    }
    if (nodo == tail && nodo == head) {
        delete nodo;
        tail= nullptr;
        head= nullptr;
    } else if (nodo == head) {
        Nodo* n_siguiente = nodo->next;
        delete nodo;
        head = n_siguiente;
        head->prev= nullptr;
    } else if (nodo == tail) {
        Nodo* n_anterior = nodo->prev;
        delete nodo;
        tail = n_anterior;
        tail->next= nullptr;
    } else {
        Nodo* n_anterior = nodo->prev;
        Nodo* n_siguiente = nodo->next;
        delete nodo;
        n_anterior->next = n_siguiente;
        n_siguiente->prev = n_anterior;
    }
}

int Lista::longitud() const {
    Nodo* nodo= head;
    int i=0;
    while(nodo != nullptr){
        nodo=nodo->next;
        i++;
    }
    return i;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* nodo =head;
    for(int j=0; j<i; j++){
        nodo=nodo->next;
    }
    return nodo->dato;
}

int& Lista::iesimo(Nat i) {
    Nodo* nodo =head;
    for(int j=0; j<i; j++){
        nodo=nodo->next;
    }
    return nodo->dato;
}

void Lista::mostrar(ostream& o) {
    o << "[";
    for(int i = 0; i < longitud()-1; i++) {
        o << iesimo(i) << ", ";
    }
    o << tail->dato << "]";
    o << endl;
}
