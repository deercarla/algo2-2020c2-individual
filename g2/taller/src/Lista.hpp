#include "Lista.h"

Lista::Lista():head(nullptr),tail(nullptr){}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    Nodo* nodo = head;
    while(nodo != NULL){
        nodo = nodo->next;
        delete(head);
        head = nodo;
    }

}

Lista& Lista::operator=(const Lista& aCopiar) {
    (*this).~Lista();
    for(int i=0; i < aCopiar.longitud(); i++){
        agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* node = new Nodo();
    node->dato = elem;
    if(longitud()==0){
        head=node;
        tail=node;
    }else if (longitud() == 1) {
        head-> prev = node;
        node->next = head;
        head = node;
        tail = node->next;
    }
    else{
        Nodo* head_vieja = head;
        node->next = this->head;
        this->head = node;
        head->prev = nullptr;
        head_vieja->prev=head;
    }
}

void Lista::agregarAtras(const int& elem) {
    Nodo* node = new Nodo();
    node->dato = elem;
    if(longitud()==0){
        head=node;
        tail=node;
    }else if (longitud() == 1) {
        tail->next = node;
        node->prev = tail;
        tail = node;
        head = node->prev;
    }
    else{
        Nodo* tail_vieja= tail;
        tail->next= node;
        this->tail = node;
        node->next = nullptr;
        node-> prev=tail_vieja;
    }
}

void Lista::eliminar(Nat i) {
    int j = 0;
    Nodo* nodo = head;
    while(j < i) {
        nodo=nodo->next;
        j++;
    }
    if (nodo == tail && nodo == head) {
        head = nullptr;
        tail = nullptr;
    } else if (nodo == head) {
        Nodo* nodo_siguiente = nodo->next;
        nodo_siguiente->prev = nullptr;
        head = nodo_siguiente;
    } else if (nodo == tail) {
        Nodo* nodo_anterior = nodo->prev;
        nodo_anterior->next = nullptr;
        tail = nodo_anterior;
    } else {
        Nodo* nodo_anterior = nodo->prev;
        Nodo* nodo_siguiente = nodo->next;
        nodo_anterior->next = nodo_siguiente;
        nodo_siguiente->prev = nodo_anterior;
    }
    delete nodo;
}

int Lista::longitud() const {
    Nodo* nodo= head;
    int i=0;
    while(nodo != nullptr){
        nodo=nodo->next;
        i++;
    }
    return i;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* nodo =head;
    for(int j=0; j<i; j++){
        nodo=nodo->next;
    }
    return nodo->dato;
}

int& Lista::iesimo(Nat i) {
    Nodo* nodo =head;
    for(int j=0; j<i; j++){
        nodo=nodo->next;
    }
    return nodo->dato;
}

void Lista::mostrar(ostream& o) {
    o << "[";
    for(int i = 0; i < longitud()-1; i++) {
        o << iesimo(i) << ", ";
    }
    o << tail->dato << "]";
    o << endl;
}

