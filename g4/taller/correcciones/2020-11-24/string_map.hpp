#include "string_map.h"
#include <set>

template <typename T>
string_map<T>::string_map(){
    _size = 0;
    _raiz = new Nodo();
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    destructorRecursivo(_raiz);
    _size = d._size;
    _raiz = copiaRecursiva(d._raiz);
    return *this;
}

template<typename T>
typename string_map<T>::Nodo* string_map<T>::copiaRecursiva(string_map::Nodo *raiz) {
    Nodo* res = new Nodo();
    if(raiz->definicion == NULL){
        res->definicion = NULL;
    }else{
        res->definicion = new T(*(raiz->definicion));
    }
    for(int i = 0; i < raiz->siguientes.size(); i++){
        if(raiz->siguientes[i] != NULL){
            res->siguientes[i] = copiaRecursiva(raiz->siguientes[i]);
        }
    }
    return res;
}


template <typename T>
string_map<T>::~string_map(){
    destructor(_raiz);
}

template<typename T>
void string_map<T>::destructor(string_map::Nodo *raiz) {
    for(Nodo* nodo : raiz->siguientes){
        if(nodo != NULL){
            destructor(nodo);
        }
    }
    if(raiz->definicion != NULL){
        delete raiz->definicion;
    }
    delete raiz;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    Nodo* actual = _raiz;
    for(char letra : clave){
        if(actual->siguientes[(int)letra] == NULL){
            actual->siguientes[(int)letra] = new Nodo();
        }
        actual = actual->siguientes[(int)letra];
    }
    if(actual->definicion == NULL){
        actual->definicion = new T();
        _size += 1;
    }
    return *(actual->definicion);
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    int res;
    Nodo* actual = _raiz;
    for(char letra : clave){
        if(actual->siguientes[(int)letra] == NULL){
            return 0;
        }
        actual = actual->siguientes[(int)letra];
    }
    if(actual->definicion == NULL){
        res = 0;
    }else{
        res = 1;
    }
    return res;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* actual = _raiz;
    for(char letra : clave){
        actual = actual->siguientes[(int)letra];
    }
    return *actual->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* actual = _raiz;
    for(char letra : clave){
        actual = actual->siguientes[(int)letra];
    }
    return *(actual->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* actual = _raiz;
    for(char letra : clave){
        actual = actual->siguientes[(int)letra];
    }
    if(actual->definicion != NULL){
        delete actual->definicion;
        _clavesT.erase(actual->_it);
    }
    actual->definicion = NULL;
    _size -= 1;
}

template <typename T>
int string_map<T>::size(){
    int res = 0;
    for(iterator it = begin(); it != end(); ++it){
        res++;
    }
    return res;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size == 0;
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& insert) {
    Nodo* actual = _raiz;
    for(char letra : insert.first){
        if(actual->siguientes[(int)letra] == NULL){
            actual->siguientes[(int)letra] = new Nodo();
        }
        actual = actual->siguientes[(int)letra];
    }

    if(actual->definicion == NULL){
        _size +=1;
    }else{
        delete actual->definicion;
        _clavesT.erase(actual->_it);
    }

    pair<string, T*> p;
    p.first = insert.first;
    p.second = new T(insert.second);

    actual->_it = _clavesT.insert(p).first;
    actual->definicion = p.second;

}



