#ifndef STRING_MAP_H_
#define STRING_MAP_H_

#include <string>
#include <set>
#include <vector>


using namespace std;

template<typename T>
class string_map{
public:
    /**
    CONSTRUCTOR
    * Construye un diccionario vacio.
    **/
    string_map();

    /**
    CONSTRUCTOR POR COPIA
    * Construye un diccionario por copia.
    **/
    string_map(const string_map<T>& aCopiar);

    /**
    OPERADOR ASIGNACION
     */
    string_map& operator=(const string_map& d);

    /**
    DESTRUCTOR
    **/
    ~string_map();

    /**
    INSERT
    * Inserta un par clave, valor en el diccionario
    **/
    void insert(const pair<string, T>& insert);

    /**
    COUNT
    * Devuelve la cantidad de apariciones de la clave (0 o 1).
    * Sirve para identificar si una clave está definida o no.
    **/

    int count(const string &key) const;

    /**
    AT
    * Dada una clave, devuelve su significado.
    * PRE: La clave está definida.
    --PRODUCE     //ALIASING--
    -- Versión modificable y no modificable
    **/
    const T& at(const string& key) const;
    T& at(const string& key);

    /**
    ERASE
    * Dada una clave, la borra del diccionario junto a su significado.
    * PRE: La clave está definida.
    --PRODUCE ALIASING--
    **/
    void erase(const string& key);

    /**
     SIZE
     * Devuelve cantidad de claves definidas */
    int size();

    /**
     EMPTY
     * devuelve true si no hay ningún elemento en el diccionario */
    bool empty() const;

    /** OPTATIVO
    * operator[]
    * Acceso o definición de pares clave/valor
    **/
    T &operator[](const string &key);

    class iterator{
    public:
        iterator(typename set<pair<string, T*>>::iterator it){
            _itSet = it;
        }

        void operator++(){
            _itSet++;
        }
        bool operator!=(const iterator& otro){
            return _itSet != otro._itSet;
        }

        pair<string, T*> operator*(){
            return *_itSet;
        }




    private:
        typename set<pair<string, T*>>::iterator _itSet;
    };

    iterator begin(){
        iterator res(_clavesT.begin());
        return res;
    }

    iterator end(){
        iterator res(_clavesT.end());
        return res;
    }

private:
    struct Nodo {
        // El constructor toma el elemento al que representa el nodo.
        Nodo(): definicion(NULL), siguientes(256,NULL){}

        vector<Nodo*> siguientes;
        T* definicion;
        typename set<pair<string, T*>>::iterator _it;
    };



    void destructorRecursivo(Nodo* raiz);
    Nodo* copiaRecursiva(Nodo* raiz);

    Nodo* _raiz;
    int _size;
    set<pair<string, T*>> _clavesT;
};



#include "string_map.hpp"

#endif // STRING_MAP_H_