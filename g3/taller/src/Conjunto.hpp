template <class T>
Conjunto<T>::Conjunto() : _raiz(NULL), _cant(0){}

template <class T>
Conjunto<T>::~Conjunto() {
    DestruirNodos(_raiz);
}

template <class T>
void Conjunto<T>::DestruirNodos(Nodo* n){
    if(n != NULL){
        DestruirNodos(n->der);
        DestruirNodos(n->izq);
        delete n;
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    if(_cant == 0){
        return false;
    }
    Nodo*  temp = _raiz;
    while(temp!=NULL){
        if(temp->valor==clave){
            return true;
        }else{
            if(temp->valor < clave){
                temp = temp->der;
            }else{
                temp = temp->izq;
            }
        }
    }
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave){
    if(_cant == 0){
        Nodo* temp = new Nodo(clave);
        _raiz = temp;
        _cant++;
    }else{
        Nodo* temp = _raiz;
        if(_cant == 1){
            if(temp->valor != clave){
                if(temp->valor < clave){
                    Nodo* nuevo = new Nodo(clave);
                    temp->der = nuevo;
                    nuevo->p = temp;
                }else{
                    Nodo* nuevo = new Nodo(clave);
                    temp->izq = nuevo;
                    nuevo->p = temp;
                }
                _cant++;
            }
        }else{
            bool found = false;
            bool fin = false;
            while(not(fin) and not(found)){
                if(temp->valor == clave){
                    found = true;
                }else{
                    if(temp->valor < clave){
                        if(temp->der == NULL){
                            fin = true;
                        }else{
                            temp = temp->der;
                        }
                    }else{
                        if(temp->izq == NULL){
                            fin = true;
                        }else{
                            temp = temp->izq;
                        }
                    }
                }
            }
            if(not found){
                if(temp->valor < clave){
                    Nodo* nuevo = new Nodo(clave);
                    temp->der = nuevo;
                    nuevo->p = temp;
                }else{
                    Nodo* nuevo = new Nodo(clave);
                    temp->izq = nuevo;
                    nuevo->p = temp;
                }
                _cant++;
            }
        }
    }
}


template <class T>
void Conjunto<T>::remover(const T& clave){
    if(_cant != 0){
        Nodo* temp = _raiz;
        bool found = false;
        bool fin = false;
        while(not(fin) and not(found)){
            if(temp->valor == clave){
                found = true;
            }else{
                if(temp->valor < clave){
                    if(temp->der == NULL){
                        fin = true;
                    }else{
                        temp = temp->der;
                    }
                }else{
                    if(temp->izq == NULL){
                        fin = true;
                    }else{
                        temp = temp->izq;
                    }
                }
            }
        }
        if(found){
            if(temp->der == NULL){
                if(temp->izq == NULL){
                    if(temp->p == NULL){
                        _raiz = NULL;
                        delete temp;
                        _cant--;
                    }else{
                        if(temp->p->izq == NULL){
                            temp->p->der = NULL;
                            delete temp;
                            _cant--;
                        }else{
                            if(temp->p->izq->valor == temp->valor){
                                temp->p->izq = NULL;
                                delete temp;
                                _cant--;
                            }else{
                                temp->p->der = NULL;
                                delete temp;
                                _cant--;
                            }
                        }
                    }
                }else{
                    if(temp->p == NULL){
                        _raiz = temp->izq;
                        temp->izq->p = NULL;
                        delete temp;
                        _cant--;
                    }else{
                        if(temp->p->izq == NULL){
                            temp->p->der = temp->izq;
                            temp->izq->p = temp->p;
                            delete temp;
                            _cant--;
                        }else{
                            if(temp->p->izq->valor == temp->valor){
                                temp->p->izq = temp->izq;
                                temp->izq->p = temp->p;
                                delete temp;
                                _cant--;
                            }else{
                                temp->p->der = temp->izq;
                                temp->izq->p = temp->p;
                                delete temp;
                                _cant--;
                            }
                        }
                    }
                }
            }else{
                if(temp->izq == NULL){
                    if(temp->p == NULL){
                        _raiz = temp->der;
                        temp->der->p = NULL;
                        delete temp;
                        _cant--;
                    }else{
                        if(temp->p->izq == NULL){
                            temp->p->der = temp->der;
                            temp->der->p = temp->p;
                            delete temp;
                            _cant--;
                        }else{
                            if(temp->p->izq->valor == temp->valor){
                                temp->p->izq = temp->der;
                                temp->der->p = temp->p;
                                delete temp;
                                _cant--;
                            }else{//El nodo es hijo derecho.
                                temp->p->der = temp->der;
                                temp->der->p = temp->p;
                                delete temp;
                                _cant--;
                            }
                        }
                    }
                }else{
                    Nodo* min = temp->der;
                    while(min->izq != NULL){
                        min = min->izq;
                    }
                    if(temp->p == NULL){
                        temp->der->p = NULL;
                        _raiz = temp->der;
                        min->izq = temp->izq;
                        temp->izq->p = min;
                        delete temp;
                        _cant--;
                    }else{//El nodo no es raiz.
                        if(temp->p->izq == NULL){
                            temp->p->der = temp->der;
                            temp->der->p = temp->p;
                            min->izq = temp->izq;
                            temp->izq->p = min;
                            delete temp;
                            _cant--;
                        }else{
                            if(temp->p->izq->valor == temp->valor){
                                temp->p->izq = temp->der;
                                temp->der->p = temp->p;
                                min->izq = temp->izq;
                                temp->izq->p = min;
                                delete temp;
                                _cant--;
                            }else{
                                temp->p->der = temp->der;
                                temp->der->p = temp->p;
                                min->izq = temp->izq;
                                temp->izq->p = min;
                                delete temp;
                                _cant--;
                            }
                        }
                    }
                }
            }
        }
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo*  temp = _raiz;
    bool found = false;
    while(temp!=NULL and not(found)){
        if(temp->valor==clave){
            found = true;
        }else{
            if(temp->valor < clave){
                temp = temp->der;
            }else{
                temp = temp->izq;
            }
        }
    }
    if(temp->der != NULL){
        Nodo* min = temp->der;
        while(min->izq != NULL){
            min = min->izq;
        }
        return min->valor;
    }else{
        if(temp->p->valor > temp->valor){
            return temp->p->valor;
        }else{
            while(temp->p->der == NULL){
                temp = temp->p;
            }
            return temp->der->valor;
        }
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* temp = _raiz;
    while(temp->izq != NULL){
        temp = temp->izq;
    }
    return temp->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* temp = _raiz;
    while(temp->der != NULL){
        temp = temp->der;
    }
    return temp->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cant;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}


