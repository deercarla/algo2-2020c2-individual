#include <iostream>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
#if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
#endif
  private:
    int mes_;
    int dia_;
};

Fecha:: Fecha(int mes, int dia):mes_(mes), dia_(dia){}

int Fecha::dia(){
    return this -> dia_;
}
int Fecha::mes(){
    return this -> mes_;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes= this->mes()==o.mes();
    return igual_dia && igual_mes;
}
#endif

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes() ;
    return os;
}
void Fecha::incrementar_dia() {
    if(dia() < dias_en_mes(mes())){
        dia_++;
    }else{
        mes_++;
        dia_ = 1;
    }
}


// Ejercicio 11, 12
// Clase Horario
class Horario {
public:
    Horario(int hora, int min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);


private:
    int hora_;
    int min_;
};

Horario:: Horario(int hora, int min):hora_(hora), min_(min){}

uint Horario::hora(){
    return this -> hora_;
}
uint Horario::min(){
    return this -> min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min() ;
    return os;
}

bool Horario::operator==(Horario h) {
    bool igual_hora = this->hora() == h.hora();
    bool igual_min= this->min()==h.min();
    return igual_hora && igual_min;
}
// Ejercicio 12
bool Horario::operator<(Horario h) {
    bool respuesta=false;
    bool menor_min= this->min()<h.min();
    if(this->hora() < h.hora()){
        respuesta=true;
    }
    if(this->hora()==h.hora()){
        respuesta=menor_min;
    }
    return respuesta;
}
// Ejercicio 13

// Clase Recordatorio

class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h, string mensaje);
    Fecha fecha();
    Horario horario();
    string mensaje();
    bool operator<(Recordatorio r);


private:
    Fecha f_;
    Horario h_;
    string msg_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string mensaje): f_(f), h_(h), msg_(mensaje) {}

string Recordatorio::mensaje(){
    return this -> msg_;
}
Fecha Recordatorio::fecha() {
    return this-> f_;
}
Horario Recordatorio::horario() {
    return this-> h_;
}

bool Recordatorio::operator<(Recordatorio r) {
   bool respuesta=false;
   if(horario()<r.horario()){
       respuesta=true;
   }
   return respuesta;
}



ostream& operator<<(ostream& os, Recordatorio r) {
    os <<r.mensaje()<<" @ "<<r.fecha() <<r.horario();
    return os;
}

// Ejercicio 14

// Clase Agenda
class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list <Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    Fecha hoy_;
    list <Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial): hoy_(fecha_inicial){}

void Agenda:: agregar_recordatorio(Recordatorio rec){
    return this-> recordatorios_.push_back(rec);
}

void Agenda::incrementar_dia() {
    return this->hoy_.incrementar_dia();
}

list <Recordatorio> Agenda::recordatorios_de_hoy() {
   list <Recordatorio> respuesta;
    for(Recordatorio r: recordatorios_){
       if(r.fecha()==hoy_){
           respuesta.push_back(r);
       }
   }
    respuesta.sort();
    return respuesta;
}

Fecha Agenda::hoy(){
    return this-> hoy_;
}

ostream& operator<<(ostream& os, Agenda a) {
    os<<a.hoy()<< endl;
    os<<"====="<< endl;
    for(Recordatorio r: a.recordatorios_de_hoy()){
        os <<  r.mensaje() << " @ " << r.fecha() <<" "<< r.horario() << endl ;
    }
    return os;
}